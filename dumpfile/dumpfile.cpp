
#include <sys/types.h>
#include <sys/stat.h>
#include "defgen.h"
#include "ficibm.h"

#define NARG 12
#define MAXNGROUPS 512

char *cmds[]={"dump","split","index","dumpsql","dumphex",(char*)NULL };
char *argu[]={"file","tam","pli","out","skip","count",
              "def","tab","sval","ival","val","iname",(char*)NULL};

void Ayuda(void){
		puts("\nUso: DUMPFILE comando {[-parametro valor]}");
		puts("    Este programa aplica una plantilla PL/I a un fichero IBM, procesandolo ");
		puts("a continuacion. Las opciones son:\n");
		puts("Comandos:");
	  printf("  %-7s         :Saca a pantalla/archivo un listado en modo ASCII.\n",cmds[0]);
	  printf("  %-7s         :Trocea el archivo en varios dependiendo de un valor\n",cmds[1]);
	  printf("  %-7s         :Indexa un archivo por uno o varios campos.\n",cmds[2]);
	  printf("  %-7s         :Genera cabeceras y CREATE TABLE para SQL*Loader de Oracle.\n",cmds[3]);
	  printf("  %-7s         :Dump hexadecimal del registro\n",cmds[4]);
		puts("Parametros:");
	  printf("  -%-5s fichero  :dsi:Fichero IBM.\n",argu[0]);
	  printf("  -%-5s tamano   :   :Tamano del registro del fichero, 0 si es variable.\n",argu[1]);
	  printf("  -%-5s plantilla:dsi:Plantilla IBM (declare) a usar con el fichero IBM.\n",argu[2]);
	  printf("  -%-5s salida   :d  :Fichero donde se listara la salida.\n",argu[3]);
	  printf("  -%-5s skip     :d  :Salta los 'skip' primeros registros.\n",argu[4]);
	  printf("  -%-5s count    :ds :Coje 'count' registros.\n",argu[5]);
	  printf("  -%-5s fich     :   :Fichero de DCL PL/I.\n",argu[6]);
	  printf("  -%-5s tabl     :   :Fichero con las Tablas OnLine.\n",argu[7]);
	  printf("  -%-5s campo    :si :Campo por el que se trocea.\n",argu[8]);
	  printf("  -%-5s valor    :d  :Valor de busqueda.\n",argu[10]);
	  printf("  -%-5s extension:si :Nombre del indice (name -> fichero.index.name).\n",argu[11]);
	  printf("  -%-5s cam[,cam]:i  :Campo/s por los que se indexa.\n\n",argu[9]);
		puts("    Se necesitan 2 variables de entorno, si no se especifica -def y -tab:");
		puts("DEF_PLI: contendra el nombre completo del fichero de definiciones PL/I.");
		puts("TAB_OL:  idem de las Tablas OnLine (ver TablaMem.exe).\n");
		puts("    Ejemplos:");
		puts("Listado completo de archivo de pacientes:");
		puts("   dumpfile dump -file cuc01r.bin -tam 451 -pli RM -out cuc01r.txt");
		puts("Crear grupos de registros dependiendo del valor de reen.rclav");
		puts("   dumpfile split -file cuo30k.bin -tam 0 -pli reen -sval reen.rclav");
		puts("Listar uno de los grupos anteriores");
		puts("   dumpfile dump -file cuo30k.bin.group.reen.rclav_e -pli reen -out salida.txt");
		puts("Crear un indice sobre fichero de pacientes, campo fhistor");
		puts("   dumpfile index -file cuc01r.bin -pli rm -tam 451 -iname nh -ival rm.fhistor");
		puts("Listar a partir de un paciente, 10 pacientes");
		puts("   dumpfile dump -file cuc01r.bin -iname nh -pli rm -ival rm.fhistor -val 300432 -count 10\n");

}

int carg(int argc, char* argv[], int* cmd,char *val[NARG]){
	if(argc==1){
		Ayuda();
		return 0;
	}

	int i,j;

	*cmd=-1;
	for(i=0;i<NARG;i++)
		val[i]=0;

	for(i=0;cmds[i];i++)
		if(!stricmp(argv[1],cmds[i]))
			*cmd=i;

	for(i=2;i<argc;i+=2)
		for(j=0;argu[j];j++)
		if(!stricmp(argv[i]+1,argu[j]))
			val[j]=argv[i+1];

	return 1;
}

int main(int argc, char *argv[]){
	int  Comando,Modo;
	char *Argumentos[NARG],fname2[256];
	/**
	 * Comprobación de parámetros:
	 */
	if( !carg(argc,argv,&Comando,Argumentos) )
		return 1;
	char *fic  =Argumentos[0],
		 *pli  =Argumentos[2],
		 *def  =Argumentos[6],
		 *tab  =Argumentos[7],
		 *sval =Argumentos[8],
		 *ival =Argumentos[9],
		 *val  =Argumentos[10],
		 *iname=Argumentos[11];
	FILE *fpout=Argumentos[3] ? fopen(Argumentos[3],"wt") : stdout,	 //Salida Normal
		 *fpout2;
	long  tam  =Argumentos[1] ? atoi(Argumentos[1]) : -1,
		  Skip =Argumentos[4] ? atoi(Argumentos[4]) : 0,
		  Count=Argumentos[5] ? atoi(Argumentos[5]) : 0;
	/**
	 * Carga de definiciones. Estas dos funcines siempre hay que llamarlas.
	 * en cualquier programa que utilice ficibm.lib
	 */
	Init( def ? def : (getenv("DEF_PLI") ? getenv("DEF_PLI") : "def") ,
		  tab ? tab : (getenv("TAB_OL" ) ? getenv("TAB_OL")  : "tab") );
	//---------------------------------------------------------------------
	if( pli==NULL ){
		printf("Error: -pli <cabecera> no definido\n");
		return -1;
	}
	//---------------------------------------------------------------------
	Modo=0;	//Dump por defecto
	switch(Comando){
	case 3: //dumpsql
		{
			Modo=1;	//Dump para SQL*Loader
			//Apertura del archivo
			CFic *vfic;
			if( sval || iname )	
				vfic=new CInd(fic,tam,iname);
			else
				vfic=new CFic(fic,tam);
			{
				//Cursor sobre el mismo
				CCur cur1(vfic);
				long End=0;
				fname2[0]=0;
				if(Argumentos[3]){
					strcpy(fname2,Argumentos[3]);
					strcat(fname2,".sql");
					fpout2=fopen(fname2,"wt");
				}else{
					fpout2=stderr;
				}
				fprintf(fpout2,"DROP TABLE %s;\n",pli);
				fprintf(fpout2,"CREATE TABLE %s (\n",pli);
					CVal val1(CVal::parse(&cur1,pli));	
					val1.DumpHeaders(fpout2,"",1);			
				fprintf(fpout2,") \n");
				fprintf(fpout2,"STORAGE (INITIAL %dM NEXT %dM PCTINCREASE 10) ;\n",
					1 + val1.campo->getTam(&cur1) * vfic->getNumReg() / 1024 / 1024 ,
					1 + val1.campo->getTam(&cur1) * vfic->getNumReg() / 1024 / 1024 / 10
					);
				fprintf(fpout2,"exit\n");
				if(fname2[0])
					fclose(fpout2);
				fprintf(fpout,"LOAD DATA INFILE * INSERT INTO TABLE %s (\n",pli);
					val1.DumpHeaders(fpout,"",2);
				fprintf(fpout,")\nBEGINDATA\n");
			}
			delete vfic;
		}
		//No hay  break para que continue con el dump
	case 0: //dump
		{
			//Apertura del archivo
			CFic *vfic;
			if( sval || iname )	
				vfic=new CInd(fic,tam,iname);
			else
				vfic=new CFic(fic,tam);
			{
				//Cursor sobre el mismo
				CCur cur1(vfic);
				long End=0;
				//Valor correpondiente.
				CVal val1(CVal::parse(&cur1,pli));	

				//Volcado de cabeceras
				if(!Modo){
					val1.DumpHeaders(fpout);			
					fprintf(fpout,"\n");
				}
				
				//Inicio de la lectura
				if(val)	cur1.busca(RegPri,1,val);
				else if(Skip)	cur1.lee( Skip );				
				else			cur1.lee( 0 );

				do{
					val1.Dump(fpout);				//Volcado del registro
					fprintf(fpout,"\n");
					if(Count)
						End= ((--Count)==0);
				}while( !End && cur1.lee(RegSig) );
			}
			delete vfic; 
		}
		break;
	case 1: //split
		{
			char naux[512],aux[128];
			CFic fic1(fic,tam);		
			CCur cur1(&fic1);					//Crear un cursor
			CVal val1(CVal::parse(&cur1,sval));

			CFic *grupo[MAXNGROUPS];
			char *grpn[MAXNGROUPS];
			long i,ngrp=0;

			cur1.lee( 0 );
			do{
				sprintf(naux,"%s_%s",sval,val1.spr(aux));
				for(i=0;i<ngrp;i++)
					if(!stricmp(grpn[i],naux))
						break;
				if(i==ngrp){
					grupo[ngrp]=fic1.CreaGrupo(naux);
					grpn[ngrp]=strdup(naux);
					ngrp++;
				}
				grupo[i]->AddReg( fic1.getOff( cur1.getRegAct() ) );
			}while( cur1.lee(RegSig) );

			for(i=0;i<ngrp;i++){
				delete grupo[i];
				free( grpn[i] );
			}
		}
		break;
	case 2:	//Indexar.
		{
			CInd ind1(fic,tam,iname,ival);
			break;
		}
	case 4: //Dump Hexadecimal
		{
			//Apertura del archivo
			CFic *vfic;
			if( sval || iname )	
				vfic=new CInd(fic,tam,iname);
			else
				vfic=new CFic(fic,tam);
			{
				//Cursor sobre el mismo
				CCur cur1(vfic);
				long End=0;
				//Valor correpondiente.
				CVal val1(CVal::parse(&cur1,pli));	

				//Volcado de cabeceras
				if(!Modo){
					val1.DumpHeaders(fpout);			
					fprintf(fpout,"\n");
				}
				
				//Inicio de la lectura
				if(val)	cur1.busca(RegPri,1,val);
				else if(Skip)	cur1.lee( Skip );				
				else			cur1.lee( 0 );

				do{
					val1.Dump(fpout,1);				//Volcado del registro
					fprintf(fpout,"\n");
					if(Count)
						End= ((--Count)==0);
				}while( !End && cur1.lee(RegSig) );
			}
			delete vfic;
		}
		break;
	}
	fclose(fpout);
	return 0;
}