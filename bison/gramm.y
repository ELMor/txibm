%{
#define YYSTYPE double
#include <math.h>
%}

%token NUM

%%

input : /* vacio */
			|	input linea
;

linea : '\n'
			| exp '\n'		{ printf("\t%.10g\n",$1); }
;

exp		: NUM					{ $$=$1;		}
			| exp exp '+'	{ $$=$1+$2;	}
			| exp exp '-'	{ $$=$1-$2;	}
;

%%

