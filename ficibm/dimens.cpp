
#include "dimens.h"
#include "campo.h"
#include "valor.h"
#include "cursor.h"

extern CCam**				lisCam;
extern ulong				nlisCam;

void 
CDim::dump(char *s,FILE *fp){
	char aux[200];
	switch(tipdim){
	case 0: 
		sprintf(aux," "); break;
	case 1: 
		if(d2==0) 
			sprintf(aux,"(%d)",d1);
		else	  
			sprintf(aux,"(%d,%d)",d1,d2);
		break;
	case 2:
		sprintf(aux,"(dummy REFER (%s))",Refer->getVar()->getNom());
		break;
	}
	if(s)
		strcpy(s,aux);
	else
		fprintf(fp,"%s",aux);
}

CDim::CDim( CCam *c, long m ){
	tipdim=2;
	Refer=c;
	cval=0;
	max=m;
}

long
CDim::getTam(CCur *pcur){
	char naux[512];
	switch( tipdim ){
	case 0: 
		return 0;  
		break;
	case 1: 
		return d1; 
		break;
	case 2:
		if( !cval ){
			Refer->getNomCom(naux,1);
			cval=new CVal(CVal::parse(pcur,naux));
		}
		if( !pcur )
			return 1;
		else{
			if( !cval->cursor ){
				cval->cursor=pcur;
				cval->proc();
			}
			long ret=(long)(*cval);
			return ret ? ret : 1 ;
		}
	}
	return -1;
}

CDim::~CDim(){
	if(cval) delete cval;
}

long
CDim::getD1(void){
	if( tipdim==2 )
		return max;
	return d1;
}

