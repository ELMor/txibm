
#ifndef __XLAT_H__
#define __XLAT_H__

#include "defgen.h"

//Translaciones EBCDIC->ASCII y Compresiones 5/8 y 3/4
//Los juegos de caracteres y tablas estan en xlat.cpp

uchar ce2a ( uchar c );			//Caracter EBCDIC -> retorna ASCII
uchar cup32( uchar c );			//Caracter 5 bits -> retorna ASCII
uchar cup40( uchar c );			//Caracter 6 bits -> retorna ASCII

void se2a (uchar *dst, uchar *org, ulong tam);	//Analogamente con cadenas de caracteres.
void sup32(uchar *dst, uchar *org, ulong tam);
void sup40(uchar *dst, uchar *org, ulong tam);

#endif
