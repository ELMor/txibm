 
#ifndef __CURSOR_H__
#define __CURSOR_H__

#include "defgen.h"
#include <stdarg.h>

#include "fichero.h"
#include "indice.h"
#define MAXTAMBUF	4096
#define MAXNUMVAL	1024
class CVal;

class CCur {
	private:
		CFic	*fiche;
		uchar	*ibuffer;				//Buffer interno
		long	regact;					//Registro actual
		CVal**	pcVal;					//Valores definidos para busquedas indice
	public:
		// Propiedades get y set
uchar*	getPReg	(void)	 { return ibuffer;	 }
long	getRegAct(void)	 { return regact;}
ulong	getRegOff(void);
		CCur	(CFic *f);				//Sobre fichero o Indice
		~CCur	();
bool	lee		(long p,long np=1);					//Lectura aleatoria
bool	busca	(long posval, int numvals=0, ...);	//buscar

friend	uchar*    CFic::getStat(CCur *cur);
friend	uchar*    CInd::getStat(CCur *cur);
friend	uchar*	  CFic::getReg(CCur *cur);
friend	bool	  CInd::busca(CCur* pCur, long PosReg, int numvals, va_list marker);

CCur&	operator=(CCur& pcright){
	regact=pcright.regact;
	return *this;
	}

};

#endif
