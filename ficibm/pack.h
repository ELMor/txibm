
#ifndef __PACK_H__
#define __PACK_H__

#include "defgen.h"

/** 
 * Esta estructura determina la ejecucion del desempaquetamiento. 
 * Este se realiza de la siguiente manera:
 * Operaciones se realizan sobre un octeto (#1) y el siguiente (#2)
 * Se hace (((#1 & mask1)<<rotl1) | ((#2 & mask2)>>rotr2)) >> rotr
 * y a continuacion se suma 'sumbase' al puntero base que sostiene los 
 * datos empaquetados. Para ver ejemplos, vease pack.cpp
 */

typedef struct {
	uchar mask1,mask2;
	uchar rotl1,rotr2,rotr;
	uchar sumbase;
} opmask;

/**
 * Funcion de Desempaquetamiento generica.
 */
void unpackgen (uchar *dst, uchar *org, ulong tam, opmask *om, int lm);
void unpack8en5(uchar *dst, uchar *org, ulong tam);
void unpack4en3(uchar *dst, uchar *org, ulong tam);
void unpackb40 (uchar *dst, uchar *org, ulong tam);

/**
 * Desempaquetar DCB con y sin signo
 */
long updcbcs(uchar *org, long nibbles_including_sign);
void updcbcs(uchar *p, long nnibb, char *dst);
long updcbss(uchar *org, long nibbles);
void updcbss(uchar *p, long nnibb, char *dst);

/**
 * Desempaquetar Binario
 */
long upbinss(uchar *org, long bytes);
long upbincs(uchar *org, long bytes);

#endif
