
#include <stdio.h>
#include <string.h>
#include "tablas.h"
#include "parse.h"
#include <sys/stat.h>
FILE *fpExport=(FILE*)NULL;
unsigned Posicion=0;

void Compila(char *s,int Modo){
	FILE *fp=fopen(s,"rt");
	if( fp==NULL ){
		perror(s);
		exit(-1);
	}
	Compila(fp,Modo);
	fclose(fp);
}

void Compila(FILE *fp,int Modo){
	fpExport=fp;
	yyparse();
	CCam::calAnchoMinimo(Modo);
	CCam::calStatics();
}

void Init(char *def, char *tab){
	struct stat st1;
	if( !def || !tab ){
		puts("No estan definidas DEF_PLI o TAB_OL.");
		exit(-1);
	}
	if( stat(def,&st1)==-1 || stat(tab,&st1)==-1){
		printf("Archivo de definicion (%s) o tablas binarias (%s)\nNO EXISTEN.",
			def,tab);
		exit(-1);
	}
	Compila(def);
	CarTabl(tab);
}

