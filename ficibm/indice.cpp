
#include <sys/types.h>
#include <sys/stat.h>
#include "indice.h"
#include "parse.h"
#include "cursor.h"
#include "valor.h"

extern FILE* fpExport;
char***	CamExp;
ulong	nCamExp;

CInd::CInd(char *nomfic, ulong treg, char *nom, char *def):CFic(nomfic,treg){
	struct stat st1,st2;
	stat(nomfic,&st1);
	char naux[256];
	fpi=fopen(strcat(strcat(strcpy(naux,nomfic),".index."),nom),"rb");
	stat(naux,&st2);
	if( fpi && st1.st_mtime>st2.st_mtime ){		//Existe pero es anterior
		carCam();								//Cargamos solo la def de camp.
		fclose(fpi);
	}else if( fpi ){							//Existe y esta OK
		carCam();
		carOor();
		loadCache();
		fclose(fpi);
		return;									//Finalizar la carga
	}else if(!fpi ){							//No existe: error
		if(def){
			//Parsing de la expresi�n de la definicion
			fpExport=tmpfile();
			fprintf(fpExport,"$DEFINDICE %s$",def);
			fseek(fpExport,0,SEEK_SET);
			campos=(char**)NULL;
			CamExp=&campos;
			nCamExp=0;
			yyparse();
			numcam=nCamExp;
			fclose(fpExport);
		}else{
			perror(naux);
			exit(-1);
		}
	}
	//Si llega a este punto, s�lo tenemos en campos la lista de campos que componen
	//el indice, falta construir los t_redi y ordenar los registros, y grabarlo todo 
	//a continuaci�n. Finalmente, se carga la cach� de registros.
	Indexa(nom);
	loadCache();
}

CInd::~CInd(){
	free(offord);
	ulong k;
	for(k=0;k<numcam;k++)
		free(campos[k]);
	free(campos);
	if(cacheado){
		for(k=0;k<tamCache;k++)
			if(cache[k])
				free(cache[k]);
		free(cache);
	}
}

static ulong	st_nc;
static tipInt*	st_tipos;
static void**	st_data;

int cmpRegInFile(const void *p1, const void *p2){
	ulong i,r1=(*(ulong*)p1)*st_nc,r2=(*(ulong*)p2)*st_nc;
	for(i=0;i<st_nc;i++)
		switch(st_tipos[i]){
			case FecRel:case EntConSig:case EntSinSig:case Binario:case EntSinSig_Binario:
			{	long ret=(long)(st_data[r1+i]) - (long)(st_data[r2+i]);
				if(ret) return ret<0 ? -1 : 1;
				break;
			}
			case Decimal:
			{	ldou ret=*(ldou*)(st_data[r1+i]) - *(ldou*)(st_data[r2+i]);
				if(ret) return ret<0 ? -1 : 1;
				break;
			}
			case FecAbs:case TexSinCom:case TexJue32:case TexJue40: case Tabla:
			{	int ret=stricmp((char*)(st_data[r1+i]),(char*)(st_data[r2+i]));
				if(ret) return ret<0 ? -1 : 1;
				break;
			}
		}
	return 0;
}

void CInd::Indexa(char *nom){
	CVal **st_vs;
	CCur  *st_cu;
	ulong i,j,k;
	st_nc=numcam;									//Numero de campos
	st_tipos=tipos;

	st_vs=(CVal**)malloc(numcam*sizeof(CVal*));		//Dos grupos de valores para comparar
	CFic f1(CFic::nombre,-1);						//Abrimos un fichero normal
	st_cu=new CCur(&f1);							//Con cursor sobre el

	for(i=0;i<numcam;i++){
		//Cada grupo de valores a un cursor
		st_vs[i]=new CVal(CVal::parse(st_cu,campos[i]));	
		tipos[i]=st_vs[i]->tipo();
	}

	offord=(ulong*)malloc(numreg*sizeof(ulong));	//Inicializamos offsets sin ordenar
	for(i=0;i<numreg;i++)
		offord[i]=i;
	printf("Cargando datos:");
	st_data=(void**)malloc(sizeof(void*)*numreg*numcam);
	for(i=0,k=0;i<numreg;i++,k++){
		if(k>=numreg/10){
			printf("+10%%");
			fflush(stdout);
			k=0;
		}
		st_cu->lee(i);
		for(j=0;j<numcam;j++){
			switch(st_tipos[j]){
				case FecRel:case EntConSig:case EntSinSig:case Binario:case EntSinSig_Binario:
				{	st_data[i*numcam+j]= (void*)(long)(*st_vs[j]);
					break;					
				}
				case Decimal:
				{	st_data[i*numcam+j]=malloc(sizeof(ldou));
					*((ldou*)st_data[i*numcam+j])=(ldou)(*st_vs[j]);
					break;
				}
				case FecAbs:case TexSinCom:case TexJue32:case TexJue40:case Tabla:
				{	st_data[i*numcam+j]=malloc(1+strlen((char*)(*st_vs[j])));
					strcpy((char*)(st_data[i*numcam+j]),(char*)(*st_vs[j]) );
					break;
				}
			}
		}
	}
	printf("\nOrdenando...\n");
	qsort(offord,numreg,sizeof(ulong),cmpRegInFile);
	printf("Ordenado.\n");
	//Borrar stuff
	for(i=0;i<numcam;i++){
		delete st_vs[i];
	}
	free(st_vs);
	delete st_cu;
	for(i=0;i<numreg;i++){
		for(j=0;j<numcam;j++){
			switch(st_tipos[j]){
				case FecRel:case EntConSig:case EntSinSig:case Binario:case EntSinSig_Binario:
				{	
					break;					
				}
				case Decimal:
				{	free( st_data[i*numcam+j] );
					break;
				}
				case FecAbs:case TexSinCom:case TexJue32:case TexJue40:case Tabla:
				{	free( st_data[i*numcam+j] );
					break;
				}
			}
		}
	}
	free(st_data);
	char aux[256];
	strcat(strcat(strcpy(aux,CFic::nombre),".index."),nom);
	fpi=fopen(aux,"wb");
	graCam();
	graOor();
	fclose(fpi);
}

int  CInd::cmpRegMemFil(int numvals, CVal** pVal, t_valor* tv){
	long k;
	for(k=0;k<numvals;k++){
		pVal[k]->proc();
		switch(pVal[k]->tipo()){
			case FecRel:case EntConSig:case EntSinSig:case Binario:case EntSinSig_Binario:
			{	long num=(long)(*pVal[k])-tv[k].ent;
				if(num) return num<0 ? -1 : 1;
				break;
			}
			case Decimal:
			{	ldou dou=(ldou)*(pVal[k])-tv[k].dec;
				if(dou) return dou<0 ? -1 : 1;
				break;
			}
			case FecAbs:case TexSinCom:case TexJue32:case TexJue40:
			{	char *str=(char*)(*pVal[k]);
				int ret=stricmp(str,(ccp)(tv[k].str));
				if(ret) return ret<0 ? -1 : 1;
				break;
			}
			case Tabla:
			{	char *str=(char*)(*(pVal[k])).cod();
				int ret=stricmp(str,((ccp)tv[k].str));
				if(ret) return ret<0 ? -1 : 1;
				break;
			}
		}
	}
	return 0;
}


bool CInd::busca(CCur* pCur,long PosReg, int numvals, va_list marker){
	static t_valor* valores=(t_valor*)NULL;
	bool ret=false;
	long aux,k;
	if(PosReg==RegPri || PosReg==RegUlt ){	// Lectura de valores de igualdad de la pila.
		valores=(t_valor*)realloc(valores,numvals*sizeof(t_valor));
		for(k=0;k<numvals;k++)
			switch(tipos[k]){
				case FecRel:case EntConSig:case EntSinSig:case Binario:case EntSinSig_Binario:
					valores[k].ent=va_arg(marker,long);
					sprintf((char*)(valores[k].str),"%ld",valores[k].ent);
					break;
				case Decimal:
					valores[k].dec=va_arg(marker,ldou);
					sprintf((char*)(valores[k].str),"%lf",valores[k].dec);
					break;
				case FecAbs:case TexSinCom:case TexJue32:case TexJue40:
					strcpy((char*)valores[k].str,(char*)va_arg(marker,char*));
					break;
				case Tabla:
					strcpy((char*)valores[k].str,(char*)va_arg(marker,char*));
					break;
			}
		//B�squeda binaria.
		ulong min=0,max=sp2,act,match=false;
		do{
			act= (min+max)/2;
			if(act>=numreg) 
				act=topreg;
			pCur->lee( act );		//Refrescar
			aux=cmpRegMemFil(numvals,pCur->pcVal,valores);
			if(aux==0){
				match=true;
				do{
					if(PosReg==RegPri)
						pCur->lee(RegAnt);
					else
						pCur->lee(RegSig);
				}while( 
					pCur->regact>0 &&
					(ulong)pCur->regact<topreg &&
					!(aux=cmpRegMemFil(numvals,pCur->pcVal,valores)) );
				if(PosReg==RegPri && (ulong)pCur->regact<topreg)
					pCur->lee(RegSig);
				else if( pCur->regact>0 )
					pCur->lee(RegAnt);
				break;
			}
			if(aux<0) min=act;
			if(aux>0) max=act;
		}while( min!=max-1 && min!=topreg && max!=0 );
		if(min==topreg)	
			return false;
		if(max==0)		
			return false;
		if(!match)
			ret = cmpRegMemFil(numvals,pCur->pcVal,valores) ? false : true;
		else
			return true;
	}else if(PosReg==RegSig){
		//Se lee el siguiente registro y se compara con los valores
		if((ulong)pCur->regact<topreg){
			pCur->lee(RegSig);
			ret = cmpRegMemFil(numvals,pCur->pcVal,valores) ? false : true;
		}else{
			ret=false;
		}
	}else if(PosReg==RegAnt){
		//Se lee el siguiente anterior y se compara con los valores
		if(pCur->regact>0){
			pCur->lee(RegAnt);
			ret = cmpRegMemFil(numvals,pCur->pcVal,valores) ? false : true;
		}else{
			ret=false;
		}
	}
	return ret;
}


void	CInd::carCam(void){
	char def[4*1024],*s=def;
	fread(&numcam,sizeof(ulong),1,fpi);
	ulong i,j;
	campos=(char**)malloc(numcam*sizeof(char*));
	for(i=0;i<numcam;i++){
		fread(&j,sizeof(ulong),1,fpi);
		campos[i]=(char*)malloc(1+j);
		fread(campos[i],1,j,fpi);
		campos[i][j]=0;
	}
	fread(tipos,sizeof(ulong),numcam,fpi);
}
void	CInd::graCam(void){
	fwrite(&numcam,sizeof(ulong),1,fpi);
	ulong i,j;
	for(i=0;i<numcam;i++){
		j=strlen(campos[i]);
		fwrite(&j,sizeof(ulong),1,fpi);
		fwrite(campos[i],1,j,fpi);
	}
	fwrite(tipos,sizeof(ulong),numcam,fpi);
}
void	CInd::carOor(void){
	offord=(ulong*)malloc(getNumReg()*sizeof(ulong));
	fread(offord,sizeof(ulong),getNumReg(),fpi);
}
void	CInd::graOor(void){
	fwrite(offord,sizeof(ulong),getNumReg(),fpi);
}

//Funcion de lectura de un registro incluyendo cach�.
ulong
CInd::getOff( ulong nr ){
	if(!offsets)
		return tamreg*offord[nr];
	else
		return offsets[offord[nr]];
}

uchar* CInd::getStat(CCur* cur){
	long resto=(cur->regact<<LEV_BTREE_CACHE) % sp2;
	if(!offsets){
		if(!resto && cacheado)		//Cache Hit!
				return cache[cur->regact>>(nbsp2-LEV_BTREE_CACHE)];
		else{
			fseek(fp,tamreg*offord[cur->regact],SEEK_SET);
			fread(cur->ibuffer,tamreg,1,fp);
			return cur->ibuffer;
		}
	}else{
		if(!resto && cacheado)	//Cache Hit!
				return cache[cur->regact>>(nbsp2-LEV_BTREE_CACHE)];
		else{
			fseek(fp,offsets[offord[cur->regact]],SEEK_SET);
			fread(cur->ibuffer,getTamBlo(fp),1,fp);
			return cur->ibuffer;
		}
	}
}

void CInd::loadCache(void){
	if(nbsp2<LEV_BTREE_CACHE){ //Menos registros de los que son cacheables.
		cacheado=false;
	}else{
		cache=(uchar**)malloc(tamCache*sizeof(uchar*));
		memset(cache,0,tamCache*sizeof(uchar*));
		ulong i,nreg,ttr;
		for(i=0;i<tamCache;i++){
			nreg=i<<(nbsp2-LEV_BTREE_CACHE);
			if(nreg>=numreg)
				break;
			if(!offsets){
				fseek(fp,offord[nreg]*tamreg,SEEK_SET);
				cache[i]=(uchar*)malloc(tamreg);
				fread(cache[i],tamreg,1,fp);
			}else{
				fseek(fp,offsets[offord[nreg]],SEEK_SET);
				ttr=getTamBlo(fp);
				cache[i]=(uchar*)malloc(ttr);
				fread(cache[i],ttr,1,fp);
			}
		}
		cacheado=true;
	}
}