#ifndef __DEFGEN_H__
#define __DEFGEN_H__

typedef unsigned long ulong;
typedef unsigned char uchar;
typedef unsigned int  uint;
typedef unsigned short ushort;
typedef const char * ccharp;
typedef long double ldou;
typedef const char * ccp;
typedef uchar* ucp;

#define MAXTAMKEY	6
#define TAMBUFFER 4*1024
#define MAXNDXFIELDS 6	//N�mero m�ximo de campos por indice.
#define MAXTAMSTR	1024

#define RegPri	-2
#define RegSig	-3
#define RegAnt	-4
#define RegUlt	-5

typedef struct {
	uchar	key[MAXTAMKEY];
	uchar*	cod;
	uchar*	des;
	uchar	keydec[MAXTAMKEY];
} reg;

typedef struct	{ 
	long	ent;	
	ldou	dec; 
	uchar	str[MAXTAMSTR];
	uchar	key[MAXTAMKEY]; 
	void	Init(char *s=0);
}		t_valor;

typedef enum {
	UnDef=0,
	FecAbs,FecRel,
	EntConSig,EntSinSig,EntSinSig_Binario,
	Binario,Binarioss,
	Decimal,
	Tabla,
	TexSinCom,TexJue32,TexJue40
} tipInt;
typedef enum { 
	NODEF=0,
	TDCL,
	TCHAr,
	TBIN,
	TBIT,
	TDEC,
	TPIC 
} Tipos;

typedef unsigned long ulong;
typedef unsigned char uchar;

#ifdef UNIX
#define stricmp strcasecmp
#endif

#endif
