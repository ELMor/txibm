
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "fichero.h"
#include "cursor.h"

CFic::CFic(int fff,char *name,char *r_name,int tr){
	fff=0;
	nombre=strdup(name);
	r_nombre=strdup(r_name);
	numreg=0;
	offsets=(ulong*)NULL;
	fp=fopen(r_name,"rb");
	tamreg=tr;
	fvirt=1;
	topreg=numreg-1;
}

//tr==0 si es de tama�o variable, tr==-1 sise cree que hay un .info
CFic::CFic(char *n, long tr){
	FILE *fpaux;

	//Comprobar que existe el fichero.
	if( (fpaux=fopen(n,"rb"))==NULL ){
		perror(n);
		exit(-1);
	}
	//Es un CRegClass (Conjunto de Registros)?
	char Head[10],*truehead="CRegClass";
	fread(Head,10,1,fpaux);
	fclose(fpaux);
	if( !stricmp(Head,truehead) ){		//Es un conjunto de registros
		abreVirt(n);
	}else{
		if(tr==-1){						//Hay que cargar del .info
			abreInfo(n);
		}else{							//Todo de la linea de comandos.
			abreIBM(n,tr);
		}
	}
	nbsp2=1+(ulong)(log(numreg)/log(2));
	sp2=1<<nbsp2;
	topreg=numreg-1;
}

void
CFic::abreInfo(char *n){
	char naux[512];
	FILE *fpoff;

	r_nombre=(char*)NULL;
	offsets=(ulong*)NULL;

	nombre=strdup(n);
	strcat(strcpy(naux,n),".info");
	fpoff=fopen(naux,"rb");

	LeeLong(fpoff,(long*)&tamreg);
	LeeLong(fpoff,(long*)&numreg);
	if(tamreg==0){
		offsets=(ulong*)malloc(numreg*sizeof(ulong));
		LeeLoAr(fpoff,numreg,(long*)offsets);
	}
	fclose(fpoff);
	if( (fp=fopen(nombre,"rb"))==NULL ){
		perror(nombre);
		exit(-1);
	}
	fvirt=0;
}

void
CFic::abreVirt(char *n){
	char Head[10];
	nombre=strdup(n);
	FILE *fpaux=fopen(nombre,"rb");
	fvirt=1;						//Es un fichero virtual
	fread(Head,10,1,fpaux);			//leer la cabecera.
	r_nombre=(char*)malloc(512);
	LeeStri(fpaux,r_nombre);
	LeeLong(fpaux,(long*)&tamreg);
	LeeLong(fpaux,(long*)&numreg);
	offsets=(ulong*)malloc(numreg*sizeof(ulong));
	LeeLoAr(fpaux,numreg,(long*)offsets);
	if( NULL==(fp=fopen(r_nombre,"rb")) ){
		perror(r_nombre);
		exit(-1);
	}
}

void 
CFic::abreIBM(char *n, long tr){
	fp=fopen(n,"rb");
	nombre=strdup(n);
	tamreg=tr;
	if( tr==0 ){				//Registros de Longitud variable?
		long tblo,treg,taux;
		offsets=(ulong*)NULL;
		numreg=0;
		while((tblo=getTamBlo(fp))>0){
			treg=0;
			do{
				offsets=(ulong*)realloc(offsets,sizeof(ulong)*(1+numreg));
				offsets[numreg++]=ftell(fp);
				taux=getTamBlo(fp);
				fseek(fp,taux-4,SEEK_CUR);
				treg+=taux;
			}while( treg<tblo-4 );
		};
	}else{						//Registros de longitud fija
		struct stat st1;
		stat(n,&st1);
		tamreg=tr;
		numreg=st1.st_size/tamreg;
		offsets=(ulong*)NULL;
	}
	r_nombre=(char*)NULL;
	fvirt=0;
	cierra();
}
	
CFic::~CFic(){
	cierra();
	fclose(fp);
	if(nombre) 
		free(nombre);
	if(r_nombre) 
		free(r_nombre);
	if(offsets) 
		free(offsets);
}

void 
CFic::cierra(void){
	if( fvirt ){	//Hay que escribir el .info
		FILE *fp2=fopen(nombre,"wb");
		fwrite("CRegClass",10,1,fp2);
		EscStri(fp2,r_nombre);
		EscLong(fp2,tamreg);
		EscLong(fp2,numreg);
		if(offsets)
			EscLoAr(fp2,numreg,(long*)offsets);
		fclose(fp2);
	}else{
		char naux[512];
		strcat(strcpy(naux,nombre),".info");
		FILE *fp2=fopen(naux,"wb");
		EscLong(fp2,tamreg);
		EscLong(fp2,numreg);
		if(offsets)
			EscLoAr(fp2,numreg,(long*)offsets);
		fclose(fp2);
	}
}

CFic*
CFic::CreaGrupo(char *gname){
	char naux[512];
	strcat(strcat(strcpy(naux,nombre),".group."),gname);
	return new CFic(0,naux,nombre,tamreg);
}

void
CFic::AddReg(ulong reg){
	if(fvirt){
		numreg++;
		offsets=(ulong*)realloc(offsets,sizeof(ulong)*numreg);
		offsets[numreg-1]=reg;
	}
}

uchar* CFic::getStat(CCur* cur){
	memset(cur->ibuffer,0,32*1024);
	if(!offsets){
		fseek(fp,cur->regact*tamreg,SEEK_SET);
		fread(cur->ibuffer,tamreg,1,fp);
	}else{
		fseek(fp,offsets[cur->regact],SEEK_SET);
		if( tamreg>0 ){
			fread(cur->ibuffer,tamreg,1,fp);
		}else{
			long tb=getTamBlo(fp);
			fread(cur->ibuffer,tb,1,fp);
		}
	}
	return cur->ibuffer;
}

ulong
CFic::getOff(ulong nr){
	ulong ret;
	if(!offsets)
		ret=nr*tamreg;
	else
		ret=offsets[nr];
	return ret;
}

uchar* CFic::getReg(CCur *cur){
	if((ulong)cur->regact>=numreg)
		return (uchar*)NULL;
	uchar *sret=getStat(cur);
	return sret;
}

bool CFic::busca(CCur* pCur,long PosReg, int numvals, va_list marker){
	puts("Busqueda en ficheros no indexados sin implementar.");
	exit(-1);
	return false;
}

