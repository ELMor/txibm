
#ifndef __FICHERO_H__
#define __FICHERO_H__

#include <stdio.h>
#include <stdarg.h>
#include "defgen.h"
#include "fmanip.h"

class CCur;			//Referencia adelantada.

class CFic {
//-----------------------------------------------------------------------------
	private:
				CFic(int fvirt,char *name,char *r_name,int tr);
		void	abreVirt(char *n);
		void	abreIBM(char *n, long tr);
		void	abreInfo(char *n);
//-----------------------------------------------------------------------------
	protected:
		FILE	*fp;
		long	tamreg;						//Tama�o registro (0:variable)
		char	*nombre;
		char	*r_nombre;
		ulong   *offsets;
		ulong	numreg,topreg;
		ulong	nbsp2,sp2;					//Siguiente potencia de 2 al tamreg
		ulong	fvirt;						//Fichero virtual (.group.)?
		ulong	getTamBlo	(FILE *f)	
			{ uchar s[4]; return fread(s,1,4,f)==4 ? s[0]*256+s[1] : 0; }
//-----------------------------------------------------------------------------
	public:
				CFic		(char* n,long lr=-1);
				~CFic		();
		void	cierra		(void);
		CFic*	CreaGrupo	(char *nombre);
		void	AddReg		(ulong offset);
		ulong	getNumReg	(void)			{ return numreg; }
		long	getTamReg   (void)			{ return tamreg; }
		uchar*	getReg		(CCur* cur);		
virtual uchar*	getStat		(CCur* cur);	
virtual ulong	getOff		(ulong nr);
virtual bool	esInd		(void)			{ return false; }
virtual long	numCamInd	(void)			{ return 0; } //En un fichero sin indexar
virtual char*	nomCamInd	(int i)			{ return (char*)NULL;}
virtual bool	busca(CCur* pCur,long PosReg, int numvals=0, va_list marker=NULL);
};

#endif
