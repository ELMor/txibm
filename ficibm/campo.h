
#ifndef __CCAMPO_H__
#define __CCAMPO_H__

#include "defgen.h"
#include "dimens.h"
#include "tipo.h"
#include "variab.h"
#include "opcion.h"

class CCam : public CObj {
	private:
		void	init(void);
		CVar*	nombre;
		CTip*	tipo;
		COpc*	opccam;
		uint	nivel;						//Nivel de anidacion en el PL1
		CCam*	padre;						//campo padre
		bool	pyc;						//Punto y coma despu�s?
		long	maximum;
		long	ContieneRefers;				//Tiene refers?
		long	OffUntMe(CCam *filio,CCur *c=(CCur*)NULL);
	public:	//Constructores
				~CCam	();
				CCam	();					
				CCam	(CVar* c, uint level);
			//Variables
		long	staticOffset;
		long	staticTamano;		//Tama�o de 1 instancia del campo
		CCam**	lhijos;
		ulong	nhijos;
			//Funciones
		CVar*	getVar	(void)			{ return nombre; }
		CDim*	getIns	(void)			{ return nombre->getIns(); }
		void	setIns	(CObj*c)		{ nombre->setIns((CDim*)c);	}
		CTip*	getTip	(void)			{ return tipo; }
		void	setTip	(CObj*c)		{ tipo=(CTip*)c; }
		COpc*	getOpc	(void)			{ return opccam; }
		void	setOpc	(CObj*c)		{ opccam=(COpc*)c; }
		bool	getPyc	(void)			{ return pyc; }
		void	setPyc	(bool l)		{ pyc=l; }
		uint	getNiv	(void)			{ return nivel; }
		CCam*	getPad	(void)			{ return padre; }
		void	setMax	(long l)		{ maximum=l; }
		long	getMax	(void)			{ return maximum; }
		void	addHij(CCam *hijo)		{ lhijos=(CCam**)realloc(lhijos,(1+nhijos)*sizeof(CCam*));
										  lhijos[nhijos++]=hijo; }
		void	getNomCom(char *s, int full=1);
		long	getTam	(CCur* c=(CCur*)NULL);				//Tama�o total
		long	getOff	(CCur* c=(CCur*)NULL);				//Offset
		long	getSTam	(void);								//Tama�o de 1 instancia

		int		EsRefer(){ 
			return nombre->getIns()->getTip()==2 ||
				(tipo->getIns() ? tipo->getIns()->getTip()==2 : 0) ; }

static	void	calAnchoMinimo(int Modo=0);
static	void	calStatics(void);

//Busca el campo 'nombre'
static	CCam*	find	(char *n,CCam* c=(CCam*)NULL,int lev=-1);	

friend	bool	operator==(const CCam&,const CCam&);
friend	bool	operator!=(const CCam&,const CCam&);
friend	bool	operator<(const CCam&,const CCam&);
friend	bool	operator>(const CCam&,const CCam&);

		void	dump	(char *s=(char*)NULL, FILE *fp=stdout);

		long	minanch;					//en caracteres
};

// Operadores necesarios para utilizar CCam en un Container STL
inline bool operator != (const CCam& c1, const CCam& c2){
	return c1.nombre->nomvar!=c2.nombre->nomvar;
}
inline bool operator == (const CCam& c1, const CCam& c2){
	return c1.nombre->nomvar==c2.nombre->nomvar;
}
inline bool operator <  (const CCam& c1, const CCam& c2){
	return c1.nombre->nomvar<c2.nombre->nomvar;
}
inline bool operator >  (const CCam& c1, const CCam& c2){
	return c1.nombre->nomvar>c2.nombre->nomvar;
}

#endif
