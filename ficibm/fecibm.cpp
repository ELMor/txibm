
#include "fecibm.h"

static inline ul g( ul y, ul m){
	return m>2 ? y : y-1 ;
}

static inline ul f( ul m ){
	return m>2 ? m+1 : m+13 ;
}

//Referidas al 01/03/1700
void fec2dias(ul y,ul m, ul d, ulp dias){
	*dias= ((ul)(365.25*g(y,m))) + ((ul)(30.6*f(m))) + d - 621049 ; 

	if( y<=1900  )
		(*dias)+=1;
	if( y<=1800  ) 
		(*dias)+=1;
}

//'dias' est� referido a 01/03/1700
void dias2fec(ul dias, ulp y, ulp m, ulp d){
	ul accu,resto;
	if (dias<=73048)		//Coreccion del 1/3/1800 al 28/2/1900
		dias-=1;
	if(dias<=36524)			//Coreccion del 1/3/1700 al 28/2/1800
		dias-=1;
	//Suponemos m<3:
	accu=621049+dias;
	*y= (ul)(accu/365.25) + 1;
	resto=(ul)(accu-365.25*(*y-1));
	*m=(ul)(resto/30.6)-13;
	resto-=(ul)(30.6*(*m+13));
	if(*m<1 || *m>12){	//Entonces es que el mes es superior a 2
		*y=(ul)(accu/365.25);
		resto=accu-(ul)(365.25*(*y));
		*m=(ul)(resto/30.6)-1;
		resto-=(ul)(30.6*(*m+1));
	}
	*d=resto;
}

void fecmasdias(ul a,ul m, ul d, ul dias, ulp as, ulp ms, ulp ds){
	ul dabs;
	fec2dias(a,m,d,&dabs); 
	dabs+=dias;
	dias2fec(dabs,as,ms,ds);
}
