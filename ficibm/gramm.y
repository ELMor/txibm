
%{

//Includes Generales
#include <ctype.h>
#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include "valor.h"

//Tipo de los tokens
#define YYSTYPE CObj*

//Fichero de donde extraer los tokens
extern FILE*	fpExport;

//Variables globales para parse de CVal y otros
extern CVal		DefVal;
extern char***	CamExp;
extern ulong	nCamExp;

//Prototipos.
void yyerror(char*s);
int  yylex(void);

//Ofrecer mas informacion si hay error
#define YYERROR_VERBOSE

//Para depurar:
//#define duts(a) puts(a)
#define duts(a) 
//#define drintf(a) printf(a); printf(" ");
#define drintf(a)

//Stack para los Refers
%}



%token NUM 
%token NOM
%token DCL			"DCL"
%token UNAL			"UNAL"
%token BASED		"BASED"
%token CHAR			"CHAR"
%token BIN			"BIN"
%token BIT			"BIT"
%token DEC			"DEC"
%token PIC			"PIC"
%token FIXED		"FIXED"
%token MASK		
%token INIT			"INIT"
%token REFER		"REFER"

%token ENTCONSIG	"ENTCONSIG"
%token ENTSINSIG	"ENTSINSIG"
%token ENTSINSIGBIN	"ENTSINSIGBIN"
%token FECABS		"FECABS"
%token FECREL		"FECREL"
%token DECIMAL		"DECIMAL"
%token TEXSINCOM	"TEXSINCOM"
%token TEXJUE32		"TEXJUE32"
%token TEXJUE40		"TEXJUE40"
%token TABLA		"TABLA"
%token BINARIO		"BINARIO"
%token BINARIOSS    "BINARIOSS"
%token MAXIMUM		"MAX"

%token DEFINDICE	"DEFINDICE"

%token EXPR			"EXPR"

%token_table

%%

program	: expr									
		| listdcl 
		| defindi
;
listdcl : dcl
		| listdcl dcl					
;
dcl		: dclhead listcam				{ $$=$2;
										   duts("1.3");						}
;
dclhead	: TNUM DCL NUM NOM dim opdcl	{ CVar *v=new CVar($4->getBuf(),
																new CDim(1));
										  int lev=atoi($3->getBuf());
										  CCam *cc=new CCam(v,lev); 
										  cc->setTip(new CTip(TDCL,(CDim*)NULL));
										  cc->setOpc(new COpc);
										  $$=cc;
										  duts("4.1");						}
;
listcam	: campo							{ $$=$1;							}
		| listcam ',' campo				{ $$=$3; duts("1.2");				}
		| listcam ';'					{ $$=$1; ((CCam*)$1)->setPyc(true);	}
;
campo	: NUM NOM dim tipo init opcam 
										{ CDim* d=(CDim*)$3;
										  CVar* v=new CVar($2->getBuf(),d);
										  uint lev=atoi($1->getBuf());
										  CCam* c=new CCam(v,lev);
										  c->setTip($4);
										  c->setOpc((COpc*)$6);
										  $$=c;								
										  duts("4.2");						}
;
init	: /* vacio */					
		| INIT '(' MASK ')'				
;
opcam	: /* vacio */					{ $$=new COpc();					}
		| ENTCONSIG						{ $$=new COpc(EntConSig);			}					
		| ENTSINSIG						{ $$=new COpc(EntSinSig);			}
		| ENTSINSIGBIN					{ $$=new COpc(EntSinSig_Binario);	}
		| FECABS						{ $$=new COpc(FecAbs);				}	
		| FECREL						{ $$=new COpc(FecRel);				}	
		| DECIMAL						{ $$=new COpc(Decimal);				}	
		| TEXSINCOM						{ $$=new COpc(TexSinCom);			}		
		| TEXJUE32						{ $$=new COpc(TexJue32);			}		
		| TEXJUE40						{ $$=new COpc(TexJue40);			}		
		| TABLA NOM						{ $$=new COpc(Tabla,$2->getBuf());	}				
		| BINARIO						{ $$=new COpc(Binario);				}		
		| BINARIOSS						{ $$=new COpc(Binarioss);			}		
;
opdcl	: /* vacio */					
		| opdcl UNAL					
		| opdcl BASED '(' NOM ')'		
		| opdcl ','						
;
tipo	: /* vacio */					{ $$=new CTip();					}
		| CHAR		dim					{ $$=new CTip(TCHAr,(CDim*)$2);		}
		| BIN FIXED dim					{ if(((CDim*)$3)->getTam()==1)
											$$=new CTip(TBIN,new CDim(15));
										  else
										    $$=new CTip(TBIN, (CDim*)$3);	}
		| DEC FIXED	dim					{ $$=new CTip(TDEC, (CDim*)$3);		}
		| BIT		dim					{ $$=new CTip(TBIT, (CDim*)$2);		}
		| PIC MASK						{ $$=new CTip(TPIC,(CDim*)NULL,$2->getBuf());}
;
dim		: /* ninguna */					{ $$=new CDim(1);					}
		| '(' NUM ')'					{ $$=new CDim(atoi($2->getBuf()));	}
		| '(' NUM ',' NUM ')'			{ $$=new CDim(atoi($2->getBuf()),
													atoi($4->getBuf()));	}
		| '(' NOM REFER '(' NOM ')' MAXIMUM NUM ')'
										{ $$=new CDim(CCam::find($5->getBuf()),
														atoi($8->getBuf()) ); }
;
TNUM	: NUM NUM NUM					
;
expr	: EXPR							
		| expr NOM						{ DefVal.campo=CCam::find($2->getBuf());
										  if(!DefVal.campo){
										    printf("%s no encontrado",$2->getBuf());
											exit(-1);
										  }									}
		| expr '(' NUM ')'				{ DefVal=DefVal[(long)atoi($3->getBuf())];}
		| expr '.' NOM					{ DefVal=DefVal[$3->getBuf()];		}
;
defindi	: DEFINDICE	ldefi
;
ldefi	: defi							{ (*CamExp)=(char**)realloc(*CamExp,
												(1+nCamExp)*sizeof(char*));
										  (*CamExp)[nCamExp]=(char*)malloc(1+
																$1->len());
										  strcpy((*CamExp)[nCamExp],
															$1->getBuf());
										  nCamExp++;						}
		| ldefi ',' defi				{ (*CamExp)=(char**)realloc(*CamExp,
												(1+nCamExp)*sizeof(char*));
										  (*CamExp)[nCamExp]=(char*)malloc(1+
																$3->len());
										  strcpy((*CamExp)[nCamExp],
															$3->getBuf());
										  nCamExp++;						}
;
defi	: NOM							{ $$=$1;							}
		| defi '.' NOM					{ $$=$1; 
										  $$->ane("."); $$->ane($3->getBuf());}
;

%%

void skipblank(FILE *fp){
	char c=' ';
	while(c==' ' || c=='\t' || c=='\n')
		c=getc(fp);
	ungetc(c,fp);
}

int yylex(){
	static bool primerDollar=false;
	char Buffer[120];
	int c;
	while(1){
		skipblank(fpExport);
		c=getc(fpExport);
		if(c=='$'){
			if(primerDollar){
				primerDollar=false;
				while( c!='\n'){
					c=fgetc(fpExport);
					if(c==EOF)
						return 0;
				}
			}else{
				primerDollar=true;
			}
			continue;
		}
		if( c=='/' ){
			c=getc(fpExport);
			if(c=='*'){
				int i=0,c1,c2;
				do{
					c1=getc(fpExport);
					c2=getc(fpExport);
					ungetc(c2,fpExport);
				}while(!(c1=='*' && c2=='/'));
				c=getc(fpExport);
				continue;
			}else{
				ungetc(c,fpExport);
				drintf("/ ");
				return '/';
			}
		}
		break;
	}
	if(isdigit(c)){
		long valor;
		ungetc(c,fpExport);
		fscanf(fpExport,"%ld",&valor);
		sprintf(Buffer,"%ld",valor);
		yylval=new CObj(Buffer);
		drintf(Buffer);
		return NUM;
	}
	if(isalpha(c)||c=='_'){
		int i=0;
		while( c!=EOF && (c=='_' || isalnum(c)) ){
			Buffer[i++]=c;
			c=getc(fpExport);
		}
		Buffer[i]=0;
		yylval=new CObj(Buffer);
		ungetc(c,fpExport);
		for(i=0;yytname[i];i++)
			if(!strncmp(Buffer,yytname[i]+1,strlen(Buffer)) &&
				*(yytname[i]+1+strlen(Buffer))=='"' ){
				drintf(Buffer);
				return yytoknum[i];
			}
		drintf(Buffer);
		return NOM;
	}
	if(c==39){ 
		int i;
		for(i=0;(c=getc(fpExport))!=39;i++)
			Buffer[i]=c;
		Buffer[i]=0;
		yylval=new CObj(Buffer);
		drintf(Buffer);
		return MASK;
	}
	if(c==EOF)
		return 0;
	return c;
}



void yyerror(char *s){
	printf("Error: %s\n",s);
}

